### LibreHeadView

*method for handless control of the locomotion of an avatar in a VR environment using gaze and attitude*

the method described here is a simple method for VR locomotion that has proven reliable, natural, easy to adopt for new VR users and reduce or make disapear the motion sickness happening in other motion control methods.
it also require less space than full room size tracking while giving space freedom illusion.

it has been developed from cave like installations to VR headsets from 2004 to 2016 and ongoing by Olivier Meunier 01[at]olm-e.be
other methods like the one from the Limsi laboratory in Paris linked and discusses in the paper (see below)

demo (to be released) using blender game engine

video showing the method here : http://f-lat.org/videos/libreheadview-demo01.webm or http://f-lat.org/videos/libreheadview-demo01.mp4

article describing the method in pdf in this repository

*( Creative Commons 4.0 Att-SA Olivier Meunier 01@olme.be 2016 )*